import random
from typing import List

from dotenv import dotenv_values, find_dotenv
from telebot import TeleBot
from telebot.types import Message, CallbackQuery

from db.models import Account_Events
from services import service
from services.service import add_user_if_not_exist, delete_user_if_exist, get_event_by_tg_identifier, show_event_str, \
    show_bill_str, get_event_statistic_by_event_tg_identifier
from telegram_constants.telegram_texts import START_WORK_MESSAGE, HELP_MESSAGE, STICKERS, UNCLEAR_MESSAGE, \
    CREATE_CHECK_MESSAGE, BACK_MESSAGE, COMPLETE_MESSAGE, ERROR_MESSAGE, OFF_MESSAGE, CREATE_EVENT_MESSAGE, \
    ALL_EVENTS_MESSAGE, CREATE_BILL_MESSAGE, ALL_BILLS_OF_EVENT, INVITE_EVENT_MESSAGE, FINISH_EVENT_MESSAGE, \
    FINISH_EVENT_MESSAGE_TO_USER
from telegram_things.telegram_markups import start_button_markup, event_button_markup
from telegram_things.telegram_menu import events_menu, show_events_menu, show_event_menu, show_bills_menu, \
    show_bill_menu

CONFIG = dotenv_values(find_dotenv(".env"))
token = CONFIG.get('TOKEN')
bot = TeleBot(token)


bills = {}


def random_sticker(message):
    """Вывод случайного"""
    bot.send_sticker(message.chat.id, STICKERS[random.randrange(0, 8)])


def markup_edit_call(markup, message, call):
    bot.edit_message_text(message, call.from_user.id, call.message.message_id,
                          parse_mode='html', reply_markup=markup)


def add_event(event: Message):
    add_user_if_not_exist(event.from_user.id)
    service.add_event(event)
    bot.send_message(event.chat.id, "Добавил")


def invite_event(message: Message):
    add_user_if_not_exist(message.from_user.id)
    service.invite_event(message.text, message.from_user.id)
    bot.send_message(message.chat.id, "Добавил")


def add_bill(bill: Message):
    add_user_if_not_exist(bill.from_user.id)
    service.add_bill(bill, bills[bill.from_user.id])
    bills.pop(bill.from_user.id)
    bot.send_message(bill.chat.id, "Добавил")


def send_event_finish_notification(sum_for_one_person, account_events: List[Account_Events], event_tg_identifier: str):
    for account in account_events:
        bot.send_message(account.account_id, FINISH_EVENT_MESSAGE_TO_USER.format(event_tg_identifier, sum_for_one_person), parse_mode='html')


@bot.message_handler(commands=['start'])
def start_command(message: Message):
    add_user_if_not_exist(message.from_user.id)
    bot.send_message(message.chat.id, START_WORK_MESSAGE.format(message.from_user.first_name), parse_mode='html',
                     reply_markup=start_button_markup())


@bot.message_handler(commands=['event'])
def event_command(message: Message):
    markup = events_menu()
    bot.send_message(message.chat.id, "Меню", parse_mode='html',
                     reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'EVENT-INVITE' == call.data.split(';')[0])
def invite_event_handle(call: CallbackQuery):
    markup_edit_call(None, INVITE_EVENT_MESSAGE, call)
    bot.register_next_step_handler_by_chat_id(call.message.chat.id, invite_event)


@bot.callback_query_handler(func=lambda call: 'GET-INVITE-EVENT' == call.data.split(';')[0])
def get_invite_event_handle(call: CallbackQuery):
    bot.send_message(call.message.chat.id, call.data.split(";")[1], parse_mode='html',
                     reply_markup=start_button_markup())


@bot.callback_query_handler(func=lambda call: 'ADD-EVENT' == call.data.split(';')[0])
def add_event_handle(call: CallbackQuery):
    markup_edit_call(None, CREATE_EVENT_MESSAGE, call)
    bot.register_next_step_handler_by_chat_id(call.message.chat.id, add_event)


@bot.callback_query_handler(func=lambda call: 'FINISH-EVENT' == call.data.split(';')[0])
def finish_event_handle(call: CallbackQuery):
    total_sum, sum_for_one_person, account_events = get_event_statistic_by_event_tg_identifier(call.data.split(';')[1])
    markup_edit_call(None, FINISH_EVENT_MESSAGE.format(call.data.split(';')[1], total_sum, sum_for_one_person), call)
    send_event_finish_notification(sum_for_one_person, account_events, call.data.split(';')[1])


@bot.callback_query_handler(func=lambda call: 'ADD-BILL' == call.data.split(';')[0])
def add_bill_handle(call: CallbackQuery):
    markup_edit_call(None, CREATE_BILL_MESSAGE, call)
    bills[call.from_user.id] = call.data.split(';')[1]
    bot.register_next_step_handler_by_chat_id(call.message.chat.id, add_bill)


@bot.callback_query_handler(func=lambda call: 'SHOW-EVENTS' == call.data.split(';')[0])
def show_events_handle(call: CallbackQuery):
    markup_edit_call(show_events_menu(service.get_all_events(call.from_user)), ALL_EVENTS_MESSAGE, call)


@bot.callback_query_handler(func=lambda call: 'SHOW-BILLS' == call.data.split(';')[0])
def show_bills_handle(call: CallbackQuery):
    markup_edit_call(show_bills_menu(service.get_all_bills(call.data.split(';')[1])), ALL_BILLS_OF_EVENT, call)


@bot.callback_query_handler(func=lambda call: 'DELETE-EVENT' == call.data.split(';')[0])
def delete_event_handle(call: CallbackQuery):
    service.delete_event_by_tg_identifier(call.data.split(';')[1])
    bot.answer_callback_query(call.id, "Deleted")
    markup_edit_call(show_events_menu(service.get_all_events(call.from_user)), ALL_EVENTS_MESSAGE, call)


@bot.callback_query_handler(func=lambda call: 'DELETE-BILL' == call.data.split(';')[0])
def delete_event_handle(call: CallbackQuery):
    service.delete_bill_by_identifier(call.data.split(';')[1])
    bot.answer_callback_query(call.id, "Deleted")
    markup_edit_call(show_events_menu(service.get_all_events(call.from_user)), ALL_EVENTS_MESSAGE, call)


@bot.callback_query_handler(func=lambda call: 'BACK-EVENTS-MENU' == call.data.split(';')[0])
def back_to_event_menu_handle(call: CallbackQuery):
    markup_edit_call(events_menu(), "Меню", call)


@bot.callback_query_handler(func=lambda call: 'BACK-BILL-MENU' == call.data.split(';')[0])
def back_to_bill_menu_handle(call: CallbackQuery):
    markup_edit_call(show_events_menu(service.get_all_events(call.from_user)), ALL_EVENTS_MESSAGE, call)


@bot.callback_query_handler(func=lambda call: 'BACK-SHOW-EVENTS' == call.data.split(';')[0])
def back_to_show_events_menu_handle(call: CallbackQuery):
    show_events_handle(call)


@bot.callback_query_handler(func=lambda call: 'EVENT' == call.data.split(';')[0])
def show_event_handle(call: CallbackQuery):
    event = get_event_by_tg_identifier(call.data.split(";")[1])
    account_event = service.get_account_event_by_event_tg_identifier(call.data.split(";")[1], call.from_user.id)
    markup_edit_call(show_event_menu(event, account_event, call.from_user.id), show_event_str(event), call)


@bot.callback_query_handler(func=lambda call: 'BILL' == call.data.split(';')[0])
def show_bill_handle(call: CallbackQuery):
    event = service.get_bill_by_identifier(call.data.split(";")[1])
    markup_edit_call(show_bill_menu(event), show_bill_str(event), call)


@bot.message_handler(content_types=['text'])
def text_message(message: Message):
    text = message.text
    if text == 'Мероприятия':
        event_command(message)
    else:
        bot.send_message(message.chat.id, UNCLEAR_MESSAGE, parse_mode='html', reply_markup=start_button_markup())
        random_sticker(message)


@bot.message_handler(commands=['off'])
def help_command(message: Message):
    delete_user_if_exist(message.from_user.id)
    bot.send_message(message.chat.id, OFF_MESSAGE.format(message.from_user.first_name), parse_mode='html')


@bot.message_handler(commands=['help'])
def help_command(message: Message):
    bot.send_message(message.chat.id, HELP_MESSAGE, parse_mode='html')


bot.infinity_polling()