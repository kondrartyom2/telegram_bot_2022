STICKERS = {
    0: 'CAACAgIAAxkBAAMoXpzP3CWCkQxsVDLY5hEUGBGOuc0AAvgAAwkSNAABLUsVuOP-dWQYBA',
    1: 'CAACAgIAAxkBAAMuXpzRGeswmw_N25IHrPx8GGGVddAAAmMDAAJS-REHdXjf1JTAZtYYBA',
    2: 'CAACAgIAAxkBAAM0XpzS1WiBaKvY7EhHRb_1dGt9w6sAAqUFAAJTsfcDr2zCCbbAFqYYBA',
    3: 'CAACAgIAAxkBAAM4XpzTIJ1-QEUEhUEga0IsvJ1uQsQAAhUAA3Xtxh-EZKYCEPTsYxgE',
    4: 'CAACAgIAAxkBAAK_NF69x5obwRkQ6seLq4FOKXnXnCEIAAISAAO0D9kVYGWn6sMwOp4ZBA',
    5: 'CAACAgIAAxkBAAM6XpzTKYWGVJW1SUqFEGj_uCEOCs4AAgEAA3Xtxh-GBXqzY9NeWhgE',
    6: 'CAACAgIAAxkBAAK_LF69x2i7IW-JgfyxAicr2TTMDGH7AAIBAAN17cYfhgV6s2PTXloZBA',
    7: 'CAACAgIAAxkBAAK_Ll69x3NtshYFo3cF0CrEBUDq1Ub1AAIPAAN17cYf_Fe6qaMBp1QZBA',
    8: 'CAACAgIAAxkBAAK_Ql69yFHRjJSk-D4nH_9DdRrRC6FdAAJcAANFqmQMQ3oxkFr5F8AZBA'
}


#language=HTML
START_WORK_MESSAGE = '<b>Привет, {}!</b>\nКуда собираешься тусить с друзьями? '

HELP_MESSAGE = "\nЯ бот разделения чеков вашей компании!"\
                  "\nЧтобы получить список и описание моих комманд введите - " + "/info" + \
                  "\nДля помощи и исправления багов обращайтесь к @kondrartyom"

#language=HTML
UNCLEAR_MESSAGE = '<b>Давай еще раз ;)</b>\nЧто ты хочешь сделать? '

#language=HTML
CREATE_CHECK_MESSAGE = '<b>Чтобы создать чек нужно:</b>\n1) Наименование товара \n2)Сумма чека '

#language=HTML
CREATE_EVENT_MESSAGE = '<b>Чтобы создать мероприятие нужно:</b>\n1) Название мероприятия'

#language=HTML
FINISH_EVENT_MESSAGE = '<b>Мероприятие {} закрыто!</b>\n<b>Общие траты: {}</b>\n<b>С одного человека: {}</b>'

#language=HTML
FINISH_EVENT_MESSAGE_TO_USER = '<b>Мероприятие {} закрыто!</b>\n<b>С тебя: {}</b>'

#language=HTML
INVITE_EVENT_MESSAGE = '<b>Чтобы присоединится к мероприятию пришлите его id -></b>\nПример: kondrartyom_event_1'

#language=HTML
CREATE_BILL_MESSAGE = '<b>Чтобы создать чек нужно:</b>\n1) Наименование товара\n2) Сумма траты'

#language=HTML
ALL_EVENTS_MESSAGE = '<b>Ваши мероприятия:</b>'

#language=HTML
ALL_BILLS_OF_EVENT = '<b>Чеки мероприятия:</b>'

#language=HTML
BACK_MESSAGE = '<b>Главное меню!</b>'

#language=HTML
COMPLETE_MESSAGE = '<b>Чек сохранен!</b>'

#language=HTML
ERROR_MESSAGE = '<b>Ошибка!</b>\nПопробуйте снова'

#language=HTML
OFF_MESSAGE = '<b>Надеюсь, был полезен</b>\nПока, {}'