from telebot.types import Message, User

from db import crud_repository
from db.models import Event, Bill


def show_event_str(event: Event):
    return "{}\n{}\n".format(event.name, event.tg_identifier)

def show_bill_str(bill: Bill):
    return "Наименование:\n{}\nСумма:\n{}\n".format(bill.name, bill.price)

def add_user(user_id: int):
    crud_repository.add_user(user_id)


def add_event(event: Message):
    name = event.text.split("1) ")[1]
    user = event.from_user
    crud_repository.add_event(name, user)


def invite_event(tg_identifier: str, user_id: str):
    crud_repository.invite_event(tg_identifier, int(user_id))


def add_bill(bill: Message, event_identifier: str):
    bill = Bill(name=bill.text.split("1) ")[1].split("2) ")[0], price=float(bill.text.split("2) ")[1]), account_id=bill.from_user.id)
    crud_repository.add_bill(bill, event_identifier)


def delete_event_by_tg_identifier(identifier: str):
    crud_repository.delete_event_by_tg_identifier(identifier)


def delete_bill_by_identifier(identifier: str):
    crud_repository.delete_bill_by_identifier(int(identifier))


def add_user_if_not_exist(user_id: int):
    user = crud_repository.get_user_by_id(user_id)
    if user is None:
        add_user(user_id)
    elif not user.is_alive:
        crud_repository.change_life_status(user_id, True)


def delete_user_if_exist(user_id: int):
    user = crud_repository.get_user_by_id(user_id)
    if user is not None:
        crud_repository.change_life_status(user_id, False)

def get_all_events(user: User):
    return crud_repository.get_all_user_events(user.id)

def get_all_bills(event_identifier: str):
    return crud_repository.get_all_event_bills(event_identifier)

def get_event_by_tg_identifier(tg_identifier: str):
    return crud_repository.get_event_by_tg_identifier(tg_identifier)

def get_bill_by_identifier(identifier: str):
    return crud_repository.get_bill_by_identifier(int(identifier))

def get_account_event_by_event_tg_identifier(tg_identifier: str, user_id: str):
    return crud_repository.get_account_event_by_event_tg_identifier_and_user_id(tg_identifier, int(user_id))

def get_event_statistic_by_event_tg_identifier(tg_identifier: str):
    bills = get_all_bills(tg_identifier)
    account_events = crud_repository.get_account_event_by_event_tg_identifier(tg_identifier)
    total_sum = 0
    for bill in bills:
        total_sum += bill.price

    sum_for_one_person = total_sum / len(account_events)

    crud_repository.finish_event_by_tg_identifier(tg_identifier)
    return total_sum, sum_for_one_person, account_events