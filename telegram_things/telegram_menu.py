import json
from typing import List

from db.models import Event, Bill, Account_Events

"""Тут мы создаем меню с кнопками не на клавиатуре"""

def create_callback_data_menus(action, choose: str = ""):
    return ";".join([action, choose])


def events_menu():
    markup = {"inline_keyboard": []}

    row = [{"text": "Добавить", "callback_data": create_callback_data_menus("ADD-EVENT")},
           {"text": "Посмотреть", "callback_data": create_callback_data_menus("SHOW-EVENTS")}]
    markup["inline_keyboard"].append(row)
    row = [{"text": "Присоединится", "callback_data": create_callback_data_menus("EVENT-INVITE")}]
    markup["inline_keyboard"].append(row)
    row = [{"text": "<<Назад", "callback_data": create_callback_data_menus("BACK")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def show_events_menu(events: List[Event]):
    markup = {"inline_keyboard": []}

    for event in events:
        if event.tg_identifier is None:
            continue
        row = [{"text": event.name, "callback_data": create_callback_data_menus("EVENT", event.tg_identifier)}]
        markup["inline_keyboard"].append(row)
    row = [{"text": "<<Назад", "callback_data": create_callback_data_menus("BACK-EVENTS-MENU")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def show_bills_menu(bills: List[Bill]):
    markup = {"inline_keyboard": []}

    for bill in bills:
        row = [{"text": bill.name, "callback_data": create_callback_data_menus("BILL", str(bill.id))}]
        markup["inline_keyboard"].append(row)
    row = [{"text": "<<Назад", "callback_data": create_callback_data_menus("BACK-BILL-MENU")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def show_event_menu(event: Event, account_event: Account_Events, user_id: str):
    markup = {"inline_keyboard": []}
    row = [{"text": "Добавить чек", "callback_data": create_callback_data_menus("ADD-BILL", event.tg_identifier)},
           {"text": "Просмотреть чеки", "callback_data": create_callback_data_menus("SHOW-BILLS", event.tg_identifier)}]
    markup["inline_keyboard"].append(row)
    row = [{"text": "Удалить", "callback_data": create_callback_data_menus("DELETE-EVENT", event.tg_identifier)}]
    markup["inline_keyboard"].append(row)
    row = [{"text": "Поделиться", "callback_data": create_callback_data_menus("GET-INVITE-EVENT", event.tg_identifier)}]
    markup["inline_keyboard"].append(row)
    if account_event.account_id == user_id:
        row = [{"text": "Завершить мероприятие",
                "callback_data": create_callback_data_menus("FINISH-EVENT", event.tg_identifier)}]
        markup["inline_keyboard"].append(row)
    row = [{"text": "<<Назад", "callback_data": create_callback_data_menus("BACK-SHOW-EVENTS")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)

def show_bill_menu(bill: Bill):
    markup = {"inline_keyboard": []}
    row = [{"text": "Удалить", "callback_data": create_callback_data_menus("DELETE-BILL", str(bill.id))}]
    markup["inline_keyboard"].append(row)
    row = [{"text": "<<Назад", "callback_data": create_callback_data_menus("BACK-SHOW-BILLS")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)
