from telebot import types


"""Тут мы занимаемся кнопками на клавиатуре, которые в свою очередь отправляют просто сообщение"""


def start_button_markup():
    """Создания кнопок на клавиатуре"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    btn1 = types.KeyboardButton('Мероприятия')
    markup.row(btn1)
    return markup


def event_button_markup():
    """Создания кнопок на клавиатуре"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    btn1 = types.KeyboardButton('Добавить')
    btn2 = types.KeyboardButton('Посмотреть')
    btn3 = types.KeyboardButton('<<Назад')
    markup.row(btn1, btn2)
    markup.row(btn3)
    return markup
