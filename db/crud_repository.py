from dotenv import dotenv_values, find_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from telebot.types import User

from db.models import Account, Event, Account_Events, Bill

CONFIG = dotenv_values(find_dotenv(".env"))
conn_string = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(CONFIG.get("DB_USER_NAME"), CONFIG.get("DB_USER_PASS"),
                                                            CONFIG.get("DB_HOST"), CONFIG.get("DB_PORT"),
                                                            CONFIG.get("DB_NAME"))
ENGINE = create_engine(conn_string, echo=False)

SessionGenerator = sessionmaker(ENGINE)


def generate_tg_identifier(username: str, event_id: int):
    return "{}_event_{}".format(username, str(event_id))


def add_event(name: str, user: User):
    with SessionGenerator() as session:
        event = Event(name=name)
        session.add(event)
        session.flush()
        event.tg_identifier = generate_tg_identifier(user.username, event.id)
        session.add(event)
        account = session.query(Account).filter(Account.id == user.id).first()
        session.add(Account_Events(account_id=account.id, event_id=event.id))
        session.commit()
        session.close()


def invite_event(tg_identifier: str, user_id: int):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == tg_identifier).first()
        user = session.query(Account).filter(Account.id == user_id).first()
        session.add(Account_Events(account_id=user.id, event_id=event.id))
        session.commit()
        session.close()


def add_bill(bill: Bill, event_identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == event_identifier).first()
        bill.event_id = event.id
        session.add(bill)
        session.commit()
        session.close()


def delete_event_by_tg_identifier(identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == identifier).first()
        event_account = session.query(Account_Events).filter(Account_Events.event_id == event.id).all()
        session.delete_all(event_account)
        session.delete(event)
        session.commit()
        session.close()


def delete_bill_by_identifier(identifier: int):
    with SessionGenerator() as session:
        bill = session.query(Bill).filter(Bill.id == identifier).first()
        session.delete(bill)
        session.commit()
        session.close()


def get_all_user_events(user_id: int):
    with SessionGenerator() as session:
        events = session.query(Event).join(Account_Events).filter(Account_Events.account_id == user_id).all()
        session.expunge_all()
        session.commit()
        session.close()
        return events

def get_all_event_bills(identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == identifier).first()
        bills = session.query(Bill).filter(Bill.event_id == event.id).all()
        session.expunge_all()
        session.commit()
        session.close()
        return bills

def get_event_by_tg_identifier(tg_identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == tg_identifier).first()
        session.expunge_all()
        session.commit()
        session.close()
        return event

def get_bill_by_identifier(identifier: int):
    with SessionGenerator() as session:
        bill = session.query(Bill).filter(Bill.id == identifier).first()
        session.expunge_all()
        session.commit()
        session.close()
        return bill

def add_user(user_id: int):
    with SessionGenerator() as session:
        session.add(Account(id=user_id))
        session.commit()
        session.close()


def get_user_by_id(user_id: int, session: sessionmaker = None) -> Account:
    create_session = False
    if session is None:
        session = SessionGenerator()
        create_session = True
    user = session.query(Account).filter_by(id=user_id).first()
    if create_session:
        session.expunge_all()
        session.commit()
        session.close()
    return user


def change_life_status(user_id: int, life_status: bool):
    with SessionGenerator() as session:
        user = get_user_by_id(user_id, session)
        if user is not None:
            user.is_alive = life_status
        session.commit()
        session.close()


def get_account_event_by_event_tg_identifier_and_user_id(tg_identifier: str, user_id: int):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == tg_identifier).first()
        account_event = session.query(Account_Events).filter(Account_Events.event_id == event.id and Account_Events.account_id == user_id).first()
        session.expunge_all()
        session.commit()
        session.close()
        return account_event


def get_account_event_by_event_tg_identifier(tg_identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == tg_identifier).first()
        account_event = session.query(Account_Events).filter(Account_Events.event_id == event.id).all()
        session.expunge_all()
        session.commit()
        session.close()
        return account_event


def finish_event_by_tg_identifier(identifier: str):
    with SessionGenerator() as session:
        event = session.query(Event).filter(Event.tg_identifier == identifier).first()
        event_account = session.query(Account_Events).filter(Account_Events.event_id == event.id).all()
        bills = session.query(Bill).filter(Bill.event_id == event.id).all()
        for bill in bills:
            session.delete(bill)
        for a_e in event_account:
            session.delete(a_e)
        session.delete(event)
        session.commit()
        session.close()


