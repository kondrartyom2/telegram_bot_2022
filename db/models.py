from dotenv import dotenv_values, find_dotenv
from sqlalchemy import create_engine, Column, BigInteger, BOOLEAN, String, Float, ForeignKey
from sqlalchemy.orm import declarative_base, relationship, backref

CONFIG = dotenv_values(find_dotenv(".env"))
conn_string = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(CONFIG.get("DB_USER_NAME"), CONFIG.get("DB_USER_PASS"),
                                                            CONFIG.get("DB_HOST"), CONFIG.get("DB_PORT"),
                                                            CONFIG.get("DB_NAME"))
ENGINE = create_engine(conn_string, echo=False)

Base = declarative_base()

class Account(Base):
    __tablename__ = 'account'

    id = Column(BigInteger, primary_key=True)
    is_alive = Column(BOOLEAN, nullable=False, default=True)
    events = relationship("Account_Events")
    bills = relationship("Bill")


class Event(Base):
    __tablename__ = 'event'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    name = Column(String, nullable=False)
    tg_identifier = Column(String, nullable=True)
    bills = relationship("Bill", backref=backref("event"))


class Account_Events(Base):
    __tablename__ = 'account_events'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    account_id = Column(BigInteger(), ForeignKey("account.id"))
    event_id = Column(BigInteger(), ForeignKey("event.id"))


class Bill(Base):
    __tablename__ = 'bill'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    name = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    event_id = Column(BigInteger, ForeignKey('event.id'))
    account_id = Column(BigInteger, ForeignKey('account.id'))


Base.metadata.create_all(ENGINE)
